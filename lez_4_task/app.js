const express = require('express')
const path = require('path')
const app = express();

const portaConnessione = 4000;          //Porta di connessione del server Express

app.use(express.static('public'))

app.listen(portaConnessione, () => {
    console.log(`Sono in ascolto sulla porta ${portaConnessione}`);
})

app.get('/', (req, res) => {
    res.sendFile(
        path.resolve(__dirname, 'index.html')
    )
})

app.get('/about', (req, res) => {
    res.sendFile(
        path.resolve(__dirname, 'about.html')
    )
})

app.get('/contact', (req, res) => {
    res.sendFile(
        path.resolve(__dirname, 'contact.html')
    )
})