const { Console } = require('console');
const http = require('http');

const hostname = '127.0.0.1';                           //localhost
const port = 4000;                                      //3000

const server = http.createServer((req, res) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/plain');
  res.end('Hello World');
}); 

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
  console.log(`Ciao sono in ascolto sulla porta ${port}`)           //Alt + 96 per Backtick
});