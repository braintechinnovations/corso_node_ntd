const express = require('express')                  //Incorporo i Moduli nello Scope Globale
const bodyparser = require('body-parser')
const ejs = require('ejs')
const mongoose = require('mongoose')
const fileupload = require('express-fileupload');

const app = express()                               //Inizializzo Express

app.use(express.static('public'))                   //Attach dei Middleware
app.use(bodyparser.json())
app.use(bodyparser.urlencoded({extended: true}))
app.use(fileupload())

app.set('view engine', 'ejs')                       //Set del Setting relativo al View Engine
const port = 4000;

mongoose.connect("mongodb://localhost:27017/blog_db", {useNewUrlParser: true})

app.listen(port, () => {
    console.log(`Sono in ascolto sulla porta ${port}`)
})

const newPostController = require('./controllers/newPost')
const homeController = require('./controllers/home')
const storePostController = require('./controllers/storePost')
const getPostController = require('./controllers/getPost')

app.get('/post/new', newPostController)
app.get('/', homeController)
app.post('/post/save', storePostController)
app.get('/post/:postId', getPostController)
