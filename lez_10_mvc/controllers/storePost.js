const BlogPost = require('../models/BlogPost')
const path = require('path')

module.exports = (req, res) => {

    let img = req.files.image;

    //lez_10_mvc/public/img
    img.mv(path.resolve(__dirname, "..", "public/img", img.name), async (err) => {
        console.log("Caricamento effettuato di " + img.name)

        await BlogPost.create({
            title: req.body.input_title, 
            body: req.body.input_body,
            image: '/img/' + img.name       //Riferimento alla risorsa statica
        })
        console.log(err);
        res.redirect('/')

    })
};