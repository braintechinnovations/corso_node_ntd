const BlogPost = require('../models/BlogPost')

module.exports = async (req, res) => {
    let postlist = await BlogPost.find({}) 
    
    res.render('index', {
        blogposts: postlist
    })
}