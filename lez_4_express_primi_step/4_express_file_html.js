const express = require('express')
const app = express();

app.use(express.static('public'))

app.listen(4000, () => {
    console.log("Sono online sulla porta 4000");
})

app.get('/about', (req, res) => {
    res.sendFile(
        path.resolve(__dirname, '4_page_about.html')
    )
})

app.get('/contactus', (req, res) => {
    res.sendFile(
        path.resolve(__dirname, '4_page_contactus.html')
    )
})

const path = require('path')

app.get('/', (req, res) => {
    res.sendFile(
        path.resolve(__dirname, '4_index.html')
    )
})