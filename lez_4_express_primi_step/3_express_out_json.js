const express = require('express')
const app = express();

app.listen(4000, () => {
    console.log("Sono online sulla porta 4000");
})

/**
 * {
 *      page: "about"
 * }
 */

app.get('/about', (req, res) => {
    var responso = {
        page: "about"
    }

    // res.end(JSON.stringify(responso))
    res.json(responso);
})

app.get('/contactus', (req, res) => {
    res.json({
        page: "contactus"
    })
})

app.get('/', (req, res) => {
    res.json({
        page: "root"
    })
})