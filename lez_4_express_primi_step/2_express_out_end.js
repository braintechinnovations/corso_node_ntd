const express = require('express')
const app = express();

app.listen(4000, () => {
    console.log("Sono online sulla porta 4000");
})

app.get('/about', (req, res) => {
   res.end("Sono la pagina About")
})

app.get('/contactus', (req, res) => {
    res.end("Sono la pagina Contatti")
})

app.get('/', (req, res) => {
    res.end("Sono la pagina principale")
})