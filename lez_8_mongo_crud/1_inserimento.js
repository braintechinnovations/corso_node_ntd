const mongoose = require('mongoose')

//Per poter utilizzare il Model, devo includerlo perché si tratta di un modulo esterno!
const BlogPost = require('./models/BlogPost')

var db = mongoose.connect(
    'mongodb://localhost:27017/blog_db',
    {useNewUrlParser: true}
)


BlogPost.create(
    {
        title: "Prova di titolo articolo due",
        body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
    }, (error, blogpost) => {
        console.log(error, blogpost)
    }
)

