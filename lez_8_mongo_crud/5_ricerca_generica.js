const mongoose = require('mongoose')

const BlogPost = require('./models/BlogPost')

mongoose.connect('mongodb://localhost/blog_db', {useNewUrlParser: true})

//FindAll
// BlogPost.find({}, (err, elenco) => {
//     console.log(elenco);
// })

//LIKE '%titolo%' ----->  /titolo/        
// BlogPost.find({
//     title: /titolo/
// }, (err, elenco) => {
//     console.log(elenco);
// })

//LIKE 'Prova%' ----->  /^Prova/        
// BlogPost.find({
//     title: /^Prova/
// }, (err, elenco) => {
//     console.log(elenco);
// })

//LIKE '%Prova' ----->  /Prova$/        
BlogPost.find({
    title: /articolo$/
}, (err, elenco) => {
    console.log(elenco);
})