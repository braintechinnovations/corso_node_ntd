const express = require('express')
const app = express()

const port = 4000;

app.listen(port, () => {
    console.log(`Sono in ascolto sulla porta ${port}`)
})