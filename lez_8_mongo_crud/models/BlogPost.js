const mongoose = require('mongoose')
const Schema = mongoose.Schema

//Definisco lo Schema sotto forma di JSON con combinazione "nome campo": "tipo campo"
const BlogPostSchema = new Schema(
    {
        title: String,
        body: String
    }
)

const BlogPost = mongoose.model('BlogPost', BlogPostSchema);

module.exports = BlogPost;