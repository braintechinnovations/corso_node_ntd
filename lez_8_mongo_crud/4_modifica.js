const mongoose = require('mongoose')
const BlogPost = require('./models/BlogPost')

mongoose.connect('mongodb://localhost/blog_db', {useNewUrlParser: true})

var id = '609e9d4f953fe40b1824670b'

BlogPost.findByIdAndUpdate(id, {
    title: "Titolo modificato del secondo documento",
}, (err, bp) => {
    console.log(err)
    console.log(bp)
})