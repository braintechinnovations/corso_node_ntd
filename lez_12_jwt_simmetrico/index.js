const express = require('express')
const jwt = require('jsonwebtoken')
const cookieparser = require('cookie-parser')

require('dotenv').config();

const app = express();
app.use(cookieparser())

const port = 4000;

app.listen(port, () => {
    console.log(`Sono in ascolto sulla porta ${port}`)
})

/** Login che mi visualizza soltanto il token per porovare su jwt.io */
// app.get('/login', (req, res) => {   
//     let payload = {
//         id: "ABCDE",
//         isLogged: true,
//         role: "USER"
//     }

//     let options = {
//         expiresIn: '100s'
//     }

//     let token = jwt.sign(payload, process.env.CHIAVE, options);
//     console.log(token)

//     res.send(token)
// })

app.get('/login', (req, res) => {   
    let payload = {
        id: "ABCDE",
        isLogged: true,
        role: "USER"
        inizializzazione: ...,
        expiring
    }

    let options = {
        expiresIn: '100s'
    }

    let token = jwt.sign(payload, process.env.CHIAVE, options);
    
    let cookiOptions = {
        expires: new Date(Date.now() + 100 * 1000),     //Aggiungo alla data, il numero di secondi convertiti in millisecondi
        httpOnly: true,
        secure: false
    }

    res.cookie('cookiecifrato', token, cookiOptions).send()
})

app.get('/profilo', (req, res) => {
    // console.log(req.cookies.cookiecifrato)

    if(!req.cookies.cookiecifrato)
        return res.status(401).json({
            status: "ERROR",
            data: "NOT_AUTH"
        });

    try{
        let payload = jwt.verify(req.cookies.cookiecifrato + "a", process.env.CHIAVE);

        //TODO: Verifica della data di expiring del Cookie JWT
        
        return res.json(payload)
    } catch(err){
        // res.json(err);
        return res.status(401).json({
            status: "ERROR",
            data: "TOKEN_NOT_INTEGER"
        });
    }

})