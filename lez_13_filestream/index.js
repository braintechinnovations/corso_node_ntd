const fs = require('fs')

const fileName = 'testo.txt'

//Lettura file ASINCRONA
fs.readFile(fileName, (err, data) => {
    if(err){
        console.log("Errore di esecuzione in lettura")
        return;
    }

    console.log("Lettura asincrona")
    console.log(data.toString());
})

//Lettura file SINCRONA
var dataInputSync = fs.readFileSync(fileName);
console.log("Lettura sincrona")
console.log(dataInputSync.toString())