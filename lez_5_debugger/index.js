const express = require('express')
const app = express()

app.listen(4000, () => {
    console.log("Server in ascolto sulla porta 4000")
})

/**
 * Per effettuare il debug, utilizzare il comando: 
 * "node --inspect-brk index.js"
 */

app.get('/', (req, res) => {

    var sommatore = 0;

    for(var i=0; i<15; i++){
        sommatore++;
    }

    res.send({
            risultato: sommatore
        })

})