const router = require('express').Router()

const contactController = require('../controllers/ContactController')

router.route("/").get((req, res) => {
    res.json({
        status: "ONLINE",
        data: ""
    })
})

router.route('/contact')
            .post(contactController.newContact)
            .get(contactController.viewContacts)

router.route('/contact/:contact_id')
            .delete(contactController.deleteContact)
            .get(contactController.viewContact)
            .put(contactController.updateContact)
            .patch(contactController.updateContact)

module.exports = router;