const mongoose = require('mongoose')
const Schema = mongoose.Schema

const ContactSchema = new Schema({
    first_name: {
        type: String,
        required: true
    },
    last_name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: false,
    },
    phone: {
        type: String,
        required: false
    },
    creation: {
        type: Date,
        default: Date.now
    }
})

module.exports = mongoose.model('Contact', ContactSchema);