const mongoose = require('mongoose')
const Contact = require('../models/Contact')


exports.newContact = async (req, res) => {
    let objContact = {
        first_name: req.body.name,
        last_name: req.body.surn,
        email: req.body.mail,
        phone: req.body.phon,
    }

    // try{
    //     let objSaved = await Contact.create(objContact);
    // }
    // catch(err){

    // }

    Contact.create(objContact, (err, objSaved) => {
        if(err){
            res.status(500).json({
                status: "ERROR",
                data: err
            })
        }
        else{
            res.json({
                status: "SUCCESS",
                data: objSaved
            })
        }

    })
}

exports.deleteContact = (req, res) => {
    Contact.findByIdAndRemove(req.params.contact_id, (err, obj) => {
        if(err){
            res.status(500).json({
                status: "ERROR",
                data: err
            })
        }
        else{
            res.status(200).json({
                status: "SUCCESS",
                data: "Eliminazione effettuata con successo!"
            })
        }
    })
}

exports.viewContact = (req, res) => {
    Contact.findById(req.params.contact_id, (err, obj) => {
        if(err){
            res.status(500).json({
                status: "ERROR",
                data: err
            })
        }
        else{
            res.status(200).json({
                status: "SUCCESS",
                data: obj
            })
        }
    })
}

exports.updateContact = (req, res) => {
    Contact.findById(req.params.contact_id, (err, obj) => {
        if(err){
            res.status(500).json({
                status: "ERROR",
                data: err
            })
        }
        else{
            obj.first_name = req.body.name;
            obj.last_name = req.body.surn;
            obj.email = req.body.emai;
            obj.phone = req.body.phon;

            //TODO: Verifica e trova un modo per modificare solo i campi passati in modo parziale!

            obj.save((err) => {
                if(err){
                    res.status(500).json({
                        status: "ERROR",
                        data: err
                    })
                }
                else{
                    res.status(200).json({
                        status: "SUCCESS",
                        data: obj
                    })
                }
            })

        }
    })
}

exports.viewContacts = (req, res) =>{
    Contact.find({}, (err, objArray) => {
        if(err){
            res.status(500).json({
                status: "ERROR",
                data: err
            })
        }
        else{
            res.status(200).json({
                status: "SUCCESS",
                data: objArray
            })
        }
    })
}

//TODO: FindALL con parametri nel Body