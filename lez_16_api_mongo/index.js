const express = require('express')
const bodyparser = require('body-parser')
const mongoose = require('mongoose')
const apiroutes = require('./routes/apiRoutes')

const app = express();

app.use(bodyparser.json())
app.use(bodyparser.urlencoded({
    extended: true
}))
app.use("/api", apiroutes)      //Attiva il router solo se la path corrisponde o inizia per "/api"

// mongoose.connect('mongodb://localhost:27017/rubrica', { useNewUrlParser: true })
mongoose.connect('mongodb+srv://utentemongo:qpzyC9Mbm6SEwEA4@cluster0.xw3ot.mongodb.net/rubrica_test?retryWrites=true&w=majority', { useNewUrlParser: true })

if(!mongoose.connection){
    console.log("Non sono riuscito a connettermi a MongoDB")
}

var port = process.env.PORT               //Necessario per la specifica della porta nel caso non sia presente nel ENV File
if(port == null || port == ""){
    port = 4000
}

app.listen(port, () => {
    console.log(`Sono in ascolto sulla porta ${port}`)
})



