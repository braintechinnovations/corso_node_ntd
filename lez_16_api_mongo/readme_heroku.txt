heroku login                    //digito qualsiasi tasto per effettuare la login sul browser.

git init                        //Inizializzao la REPO con git
git add .                       //Metto tutti i miei file nella staging area
git commit -m "Primo commit"    //rendo permanente in locale il mio versionamento e lo preparo per la push

heroku create                   //Creo un "contenitore" su heroku (e riservo un indirizzo web)

git push heroku master          //Invio il mio commit (di prima) sul server di heroku e viene subito letto il file Procfile che avvia la costruzione del server.