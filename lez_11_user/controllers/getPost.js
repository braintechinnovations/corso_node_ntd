const BlogPost = require('../models/BlogPost')

module.exports = async (req, res) => {
    // console.log(req.params);

    let bp = await BlogPost.findById(req.params.postId);
    res.render('post', {
        blogpost: bp                 
    })
}