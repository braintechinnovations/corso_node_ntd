const mongoose = require('mongoose')
const bcrypt = require('bcrypt')
const Schema = mongoose.Schema;

const UserSchema = new Schema(
    {
        username: {
            type: String,
            required: true,
            unique: true            //Permesso solo un elemento con questo campo
        },
        password: {
            type: String,
            required: true
        }
    }
);

UserSchema.pre('save', function(next){
    const user = this

    user.username = user.username.toUpperCase()

    bcrypt.hash(user.password, 12, (err, passwordHash) =>{
        user.password = passwordHash
        next()
    })
})

const User = mongoose.model('User', UserSchema)
module.exports = User