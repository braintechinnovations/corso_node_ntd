const User = require('../models/UserModel')

module.exports = (req, res, next) => {

    if(!req.session.userId)                 //Se non esiste faccio il redirect
        return res.redirect('/auth/login')

    // next()

    User.findById(req.session.userId, (err, objUser) => {       //TODO: Verifica funzionamento. 
                                                                //Se esiste, verifico che c'è ancora sul DB
        if(err || !objUser){

            return res.redirect('/auth/login')

        }

        next()
    })

}