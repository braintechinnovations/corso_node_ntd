//Se sei autenticato, su questa pagina non puoi entrare!

module.exports = (req, res, next) => {
    if(req.session.userId){                  //Significa che sei loggato
        return res.redirect("/")
    }

    next();
}