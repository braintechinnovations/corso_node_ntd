const http = require('http')
const fs = require('fs')                            //Modulo per la manipolazione del filesystem

const homePage = fs.readFileSync('3_index.html')
const aboutPage = fs.readFileSync('3_page_about.html')
const contactPage = fs.readFileSync('3_page_contactus.html')
const notFoundPage = fs.readFileSync('3_not_found.html')

const server = http.createServer((req, res) => {

    // if(req.url == '/about'){
    //     res.end(aboutPage)
    // } else if(req.url == '/contactus'){
    //     res.end(contactPage)
    // } else if(req.url == '/'){
    //     res.end(homePage)
    // } else{
    //     res.writeHead(404)
    //     res.end(notFoundPage)
    // }

    switch(req.url){
        case '/about':
            res.end(aboutPage)
            break;
        case '/contactus':
            res.end(contactPage)
            break;
        case '/':
            res.end(homePage)
            break;
        default:
            res.writeHead(404)
            res.end(notFoundPage)
            break;
    }

})

server.listen(4000);