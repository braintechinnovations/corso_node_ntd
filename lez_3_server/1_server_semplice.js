const http = require('http')

const server = http.createServer((req, res) => {
    console.log(req.url);                           //Output lato server.

    res.end(req.url);                               //Output restituito al client sotto forma di response.
})

server.listen(4000);