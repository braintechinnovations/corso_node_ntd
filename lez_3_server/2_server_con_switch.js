const { createSecretKey } = require('crypto');
const http = require('http')

const server = http.createServer((req, res) => {
    if(req.url == '/about'){
        res.end("Pagina chi siamo")
    } else if(req.url == '/contactus'){
        res.end("Pagina contatti")
    } else if(req.url == '/'){
        res.end("Pagina principale")
    } else{
        res.writeHead(404)
        res.end("Pagina non trovata, 404!")
    }
})

server.listen(4000);