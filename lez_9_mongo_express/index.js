const express = require('express')

const app = express()
app.use(express.static('public'))

const ejs = require('ejs')
app.set('view engine', 'ejs')

const bodyParser = require('body-parser')
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))

const port = 4000
app.listen(port, () => {
    console.log(`Sono connesso alla porta ${port}`)
}) 

app.get('/about', (req, res) => {
    res.render('about')
})

app.get('/contact', (req, res) => {
    res.render('contact')
})

// app.get('/post', (req, res) => {
//     res.render('post')
// })

app.get('/post/new', (req, res) => {
    res.render('create')
})

const mongoose = require('mongoose')
const BlogPost = require('./models/BlogPost')

mongoose.connect("mongodb://localhost:27017/blog_db", {useNewUrlParser: true})

// app.post('/post/save', (req, res) => {

//     var nuovoOggetto = {
//         title: req.body.input_title, 
//         body: req.body.input_body
//     }

//     BlogPost.create(nuovoOggetto, (err, bp) => {
//         console.log(err);
//         console.log(bp);
//         res.redirect('/')
//     })
    
// })

//Post senza immagine
// app.post('/post/save', async (req, res) => {
//     await BlogPost.create({
//         title: req.body.input_title, 
//         body: req.body.input_body
//     })
//     res.redirect('/')
// })

const fileUpload = require('express-fileupload');
app.use(fileUpload())

const path = require('path')

app.post('/post/save', (req, res) => {

    let img = req.files.image;

    img.mv(path.resolve(__dirname, "public/img", img.name), async (err) => {
        console.log("Caricamento effettuato di " + img.name)

        await BlogPost.create({
            title: req.body.input_title, 
            body: req.body.input_body,
            image: '/img/' + img.name       //Riferimento alla risorsa statica
        })
        console.log(err);
        res.redirect('/')

    })
})

app.get('/', async (req, res) => {
    
    let postlist = await BlogPost.find({})
    
    res.render('index', {
        blogposts: postlist
    })

})

//Rotta di default per evitare la post senza ID
app.get('/post', (req, res) => {
    res.redirect('/')
})

app.get('/post/:postId', async (req, res) => {
    // console.log(req.params);

    let bp = await BlogPost.findById(req.params.postId);
    res.render('post', {
        blogpost: bp                 
    })
})