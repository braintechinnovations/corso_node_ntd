const BlogPost = require('../models/BlogPost')

module.exports = async (req, res) => {
    let postlist = await BlogPost.find({}).populate('userid')

    res.render('index', {
        blogposts: postlist
    })
}