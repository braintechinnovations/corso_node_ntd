const User = require('../models/UserModel')

module.exports = (req, res) => {
    User.create({
        username: req.body.input_username,
        password: req.body.input_password
    }, (err, usr) => {
        if(err) {
            return res.redirect('/auth/register')
        }

        res.redirect('/')
    })
}