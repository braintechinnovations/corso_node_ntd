const BlogPost = require('../models/BlogPost')
const path = require('path')

module.exports = async (req, res) => {

    if(!req.files){
        await BlogPost.create({
            title: req.body.input_title, 
            body: req.body.input_body,
            image: '',
            userid: req.session.userId
        })
        res.redirect('/')
    }
    else{
        let img = req.files.image;

        img.mv(path.resolve(__dirname, "..", "public/img", img.name), async (err) => {
            console.log("Caricamento effettuato di " + img.name)
    
            await BlogPost.create({
                title: req.body.input_title, 
                body: req.body.input_body,
                image: '/img/' + img.name,       //Riferimento alla risorsa statica
                userid: req.session.userId
            })
            console.log(err);
            res.redirect('/')
    
        })
    }
    
};