const User = require('../models/UserModel')
const bcrypt = require('bcrypt')

module.exports = (req, res) => {

    let user = {
        username: req.body.input_username,
        password: req.body.input_password
    }

    User.findOne({
        username: user.username
    }, (err, userObj) => {
        if(userObj){
            bcrypt.compare(user.password, userObj.password, (err, same) => {
                if(same){
                    //res.send("Utente trovato e password corrispondente!")
                    //TODO: Crea una sessione sul server e salva un Cookie all'interno del Cliente per identificarla
                    req.session.userId = userObj._id;

                    //TODO: Creazione del token JWT con la sola informazione del User ID

                    res.redirect("/")
                }
                else{
                    //res.send("Utente trovato ma la password non corrisponde!")
                    res.redirect("/auth/login")
                }
            })
        }
        else{
            //res.send("Errore, utente non trovato!")
            res.redirect("/auth/login")
        }
    })

}
