const express = require('express')                  //Incorporo i Moduli nello Scope Globale
const bodyparser = require('body-parser')
const ejs = require('ejs')
const mongoose = require('mongoose')
const fileupload = require('express-fileupload');
const expressSession = require('express-session')

const app = express()                               //Inizializzo Express

app.use(express.static('public'))                   //Attach dei Middleware
app.use(bodyparser.json())
app.use(bodyparser.urlencoded({extended: true}))
app.use(fileupload())
app.use(expressSession({
    secret: "cicciopasticcio",                       //Chiave segreta di criptazione (salt) degli identificatori di sessione
    resave: false,
    saveUninitialized: true,
    cookie: { 
        secure: 'auto', 
        maxAge: 3600000 
    },
}))

global.loggedIn = null;

app.use("*", (req, res, next) => {
    loggedIn = req.session.userId;
    next();
})

app.set('view engine', 'ejs')                       //Set del Setting relativo al View Engine
const port = 4000;

mongoose.connect("mongodb://localhost:27017/blog_db", {useNewUrlParser: true})

app.listen(port, () => {
    console.log(`Sono in ascolto sulla porta ${port}`)
})

const redirectifLoggedMiddleware = require('./middleware/redirectifLoggedMiddleware');
const redirectIfUserNotExisting = require('./middleware/redirectIfUserNotExisting');
const ValidatePostMiddleware = require('./middleware/ValidatePostMiddleware');

const newPostController = require('./controllers/newPost')              //Sezione post
const homeController = require('./controllers/home')
const storePostController = require('./controllers/storePost')
const getPostController = require('./controllers/getPost')

app.get('/post/new', redirectIfUserNotExisting, newPostController)
app.get('/', homeController)
app.post('/post/save', [redirectIfUserNotExisting, ValidatePostMiddleware], storePostController)
app.get('/post/:postId', getPostController)

const newUserController = require('./controllers/newUser')              //Sezione utente
const storeUserController = require('./controllers/storeUser')
const loginUserController = require('./controllers/login')
const checkLoginController = require('./controllers/loginCheck')
const logoutUserController = require('./controllers/logout');
const errorController = require('./controllers/error');

app.get('/auth/register', redirectifLoggedMiddleware, newUserController)
app.post('/auth/save', redirectifLoggedMiddleware, storeUserController)
app.get('/auth/login', redirectifLoggedMiddleware, loginUserController)
app.post('/auth/login', redirectifLoggedMiddleware, checkLoginController)
app.get('/auth/logout', logoutUserController)

app.get('/error', errorController)      //TODO: Flash

app.use((req, res) => {
    res.render('error404');
})