const mysql = require('mysql')

const connection = mysql.createConnection(
    {
        host: "127.0.0.1",
        port: 3306,
        user: "root",
        password: "toor",
        database: "rubrica_test"
    }
)

connection.connect(
    function(outError){
        if(outError) throw outError;

        var varNome = "Valeria";
        var varCognome = "Verdi";
        var varTelefono = "123456";

        var sql = `INSERT INTO rubrica (nome, cognome, telefono) VALUES ("${varNome}", "${varCognome}", "${varTelefono}")`;

        connection.query(sql, function(err){
            if(err) throw err;

            console.log("Sono riuscito ad inserire una riga.")
        })
    }
)