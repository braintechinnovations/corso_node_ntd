const mysql = require('mysql')

const connection = mysql.createConnection(
    {
        host: "127.0.0.1",
        port: 3306,
        user: "root",
        password: "toor",
        database: "rubrica_test"
    }
)

connection.connect(
    function(outError){
        if(outError) throw outError;

        var nome = "Giovanni";

        var sql = `SELECT * FROM rubrica WHERE nome = "${nome}"`;

        let arrayObj = [];

        connection.query(sql, function(err, result){
            if(err) throw err;

            Object.keys(result).forEach(function(index){
                let temp = result[index];

                let tempObj = {
                    nome_persona: temp.nome,
                    cogn_persona: temp.cognome,
                    tele_persona: temp.telefono
                }

                arrayObj.push(tempObj);

                console.log(arrayObj);      //Attenzione, solo qui possiamo direttamente stampare l'array
            })
        })

        console.log(arrayObj);      //Ops, ho stampato prima che l'array venga popolato dalla richiesta asincrona

    }
)