const mysql = require('mysql')

const connection = mysql.createConnection(
    {
        host: "127.0.0.1",
        port: 3306,
        user: "root",
        password: "toor",
        database: "rubrica_test"
    }
)

connection.connect(
    function(outError){
        if(outError) throw outError;

        var sql = "CREATE TABLE rubrica ( " + 
                        "id INTEGER AUTO_INCREMENT PRIMARY KEY," + 
                        "nome VARCHAR(250), " + 
                        "cognome VARCHAR(250), " + 
                        "telefono VARCHAR(25)" +
                    ")";

        connection.query(sql, function(err, results){
            if(err) throw err;

            console.log("Ho effettuato l'operazione di creazione della tabella");
        })
    }
)