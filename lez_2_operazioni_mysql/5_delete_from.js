const mysql = require('mysql')

const connection = mysql.createConnection(
    {
        host: "127.0.0.1",
        port: 3306,
        user: "root",
        password: "toor",
        database: "rubrica_test"
    }
)

connection.connect(
    function(outError){
        if(outError) throw outError;

        var identifier = 5;

        var sql = `DELETE FROM rubrica WHERE id = ${identifier}`;

        connection.query(sql, function(err, result){
            if(err) throw err;

            console.log("Ho eliminato la riga");
            console.log(result);
        })
    }
)