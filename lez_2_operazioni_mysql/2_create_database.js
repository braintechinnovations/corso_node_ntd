const mysql = require('mysql')

const connection = mysql.createConnection(
    {
        host: "127.0.0.1",
        port: 3306,
        user: "root",
        password: "toor"
    }
)

connection.connect(
    function(outError){
        if(outError) throw outError;

        var sql = "CREATE DATABASE rubrica_test_due"

        connection.query(sql, function(err){
            if(err) throw err

            console.log("Tutto ok!")
        })
    }
)