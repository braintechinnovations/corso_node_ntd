const express = require('express')
const ejs = require('ejs')

const porta = 4000; 
const app = express() 

app.use(express.static('public'))
app.set('view engine', 'ejs')


app.listen(porta, () => {
    console.log(`Sono connesso alla porta ${porta}`)
})

app.get('/', (req, res) => {
    res.render('index')
})

app.get('/about', (req, res) => {
    res.render('about')
})

app.get('/contact', (req, res) => {
    res.render('contact')
})

app.get('/post', (req, res) => {
    res.render('post')
})