//Generazione della chiave privata RSA
//openssl genrsa -out rsa.pri 1024

//Generazione della chiave pubblica RSA
//openssl rsa -in rsa.pri -out rsa.pub -pubout -outform PEM

const express = require('express')
const fs = require('fs')                //Modulo per la lettura dei file sul FileSystem
const cookieparser = require('cookie-parser')
const jwt = require('jsonwebtoken')

const app = express()
app.use(cookieparser())

const port = 4000;

app.listen(port, () => {
    console.log(`Sono in ascolto sulla porta ${port}`)
})

app.get('/login', (req, res) => {
    let payload = {
        id: "ABCDE",
        isLogged: true,
        role: "USER"
    }

    let privateKey = fs.readFileSync('rsa.pri')

    let jwtOptions = {
        expiresIn: '100s',
        algorithm: 'RS256'
    }

    let token = jwt.sign(payload, privateKey, jwtOptions)
    
    let cookieOptions = {
        expires: new Date(Date.now() + 100 * 1000),
        httpOnly: true,
        secure: false
    }
    res.cookie('cookiersa', token, cookieOptions).send()
})

app.get('/profilo', (req, res) => {
    if(!req.cookies.cookiersa)
        return res.status(401).json({
            status: "ERROR",
            data: "NOT_AUTH"
        })

    let publicKey = fs.readFileSync('rsa.pub')

    let jwtOptions = {
        algorithm: 'RS256'
    }

    try{
        let payload = jwt.verify(req.cookies.cookiersa, publicKey, jwtOptions);

        //TODO: Verifica della data di ExpiresIn

        res.send(payload)
    } catch(err){
        return res.status(401).json({
            status: "ERROR",
            data: err
        })
    }
    
})

const jwtrsamiddleware = require('./middleware/jwtRsaCheckMiddleware')

app.get('/profilo', jwtrsamiddleware, (req, res) => {
    res.json({
        status: "success"
    })
})