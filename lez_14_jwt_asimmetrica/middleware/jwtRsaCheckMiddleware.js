const fs = require('fs')
const jwt = require('jsonwebtoken')

module.exports = (req, res, next) => {
    if(!req.cookies.cookiersa)
        return res.status(401).json({
            status: "ERROR",
            data: "NOT_AUTH"
        })

    let publicKey = fs.readFileSync('rsa.pub')

    let jwtOptions = {
        algorithm: 'RS256'
    }

    try{
        jwt.verify(req.cookies.cookiersa, publicKey, jwtOptions);
        
        next();                         //Solo momento in cui va in next!
    } catch(err){
        return res.status(401).json({
            status: "ERROR",
            data: err
        })
    }
}